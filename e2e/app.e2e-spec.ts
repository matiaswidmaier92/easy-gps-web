import { EasyGpsWebPage } from './app.po';

describe('easy-gps-web App', () => {
  let page: EasyGpsWebPage;

  beforeEach(() => {
    page = new EasyGpsWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
