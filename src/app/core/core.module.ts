import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from '@angular/router';

import { HeaderComponent } from "app/core/header/header.component";
import { HomeComponent } from "app/core/home/home.component";

@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    RouterModule,
    CommonModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [
  ]
})
export class CoreModule {}