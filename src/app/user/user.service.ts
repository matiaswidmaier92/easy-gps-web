import { Http, Response, Headers } from "@angular/http";
import { Injectable, EventEmitter } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from "rxjs";

import { User } from "app/user/user.model";

@Injectable()
export class UserService {
  private users: User[]

  constructor(private http: Http) { }

  getUsers() {
    return this.http.get('http://localhost:3001/user')
      .map((response: Response) => {
        const users =  response.json();
        let transformatedUsers: User[] = [];
        for (let usr of users){
          transformatedUsers.push(new User(
            usr.username,
            '',
            usr.name,
            usr.lastname,
            usr.role
          ));
        }
        this.users = transformatedUsers;
        return transformatedUsers;
      }

      )
  }

}
