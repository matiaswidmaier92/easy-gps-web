import { Component, OnInit } from '@angular/core';

import { UserService } from "app/user/user.service";
import { User } from "app/user/user.model";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(
        (users: User[]) => {
          this.users = users
        }
      );
  }

}
