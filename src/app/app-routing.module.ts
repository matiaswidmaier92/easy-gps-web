import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninComponent } from './auth/signin/signin.component';
import { GpsPointListComponent } from "app/gps/gps-point-list/gps-point-list.component";
import { UserListComponent } from "app/user/user-list/user-list.component";
import { AuthGuardService } from "app/auth/auth-guard.service";


const appRoutes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'user', component: UserListComponent, canActivate: [AuthGuardService]},
  { path: 'gpspointlist', component: GpsPointListComponent, canActivate: [AuthGuardService]}
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
