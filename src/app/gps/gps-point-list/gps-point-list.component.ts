import { Component, OnInit, ViewChild } from '@angular/core';

import { NgForm } from '@angular/forms';
import { GpsPointService } from 'app/gps/gps.service';
import { GpsPoint } from 'app/gps/gpsPoint.model';
import { DatePipe } from '@angular/common';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';

@Component({
  selector: 'app-gps-point-list',
  templateUrl: './gps-point-list.component.html',
  styleUrls: ['./gps-point-list.component.css']
})
export class GpsPointListComponent implements OnInit {
  @ViewChild('f') filterForm: NgForm;
  private points: GpsPoint[];

  constructor(private gpspointService: GpsPointService) {}

  title: string = 'My first angular2-google-maps project';
  lat: number = -32.313433333333336;
  lng: number = -58.0574;
  startDate: Date;
  endDate: Date;
  
  ngOnInit() {
    /*this.filterForm.controls['startTime'].setValue("00:00");
    this.filterForm.controls['endTime'].setValue("23:59");*/
  }

  onSubmit() {
    this.startDate = new Date(this.filterForm.value['startDate']);
    let hs = this.filterForm.value['startTime'].split(":")[0]
    let min = this.filterForm.value['startTime'].split(":")[1]
    if(hs != "" && min)
      this.startDate.setHours(hs, min);
    else
      this.startDate.setHours(0, 0);

    this.endDate = new Date(this.filterForm.value['endDate']);
    hs = this.filterForm.value['endTime'].split(":")[0]
    min = this.filterForm.value['endTime'].split(":")[1]
    if(hs != "" && min)
      this.endDate.setHours(hs, min);
    else
      this.endDate.setHours(0, 0);
    this.gpspointService.getPoints(this.startDate, this.endDate)
      .subscribe(
        (gpsPoints: GpsPoint[]) => {
          this.points = gpsPoints;
        }
      );
  }

}
