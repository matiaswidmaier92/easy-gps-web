import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpsPointListComponent } from './gps-point-list.component';

describe('GpsPointListComponent', () => {
  let component: GpsPointListComponent;
  let fixture: ComponentFixture<GpsPointListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsPointListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsPointListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
