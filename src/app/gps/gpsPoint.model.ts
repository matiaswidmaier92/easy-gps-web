export class GpsPoint {
  constructor(
    public trackerId: number,
    public date: Date,
    public lat: number, 
    public lng: number,
    public speed: number
  ) {}
}