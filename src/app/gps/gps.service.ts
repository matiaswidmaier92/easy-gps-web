import { Http, Response, Headers } from "@angular/http";
import { Injectable, EventEmitter } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from "rxjs";

import { GpsPoint } from "app/gps/gpsPoint.model";

@Injectable()
export class GpsPointService {
  private gpsPoints: GpsPoint[];

  constructor(private http:Http) { }

  getPoints(startDate:Date, endDate:Date) {
    return this.http.get(
      `http://localhost:3001/gpspoint?startDate=`+ startDate.valueOf() +
      `&endDate=` + endDate.valueOf() + 
      `&trackerId=1000107&startSpeed=0&endSpeed=1000`)
    .map((response:Response) => {
      const points = response.json();
      let transformatedPoints: GpsPoint[] = [];
      for (let pto of points){
        transformatedPoints.push(new GpsPoint(
          pto.trackerId,
          new Date(pto.date),
          pto.point.coordinates[0],
          pto.point.coordinates[1], 
          pto.speed));        
      }
    this.gpsPoints = transformatedPoints;
    return transformatedPoints;
    });
  }
}
