import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { User } from "app/user/user.model";
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  token: string;

  constructor(private router:Router, private http: Http) {}

  signupUser(email: string, password: string) {
  }

  signinUser(user: User) {
    const body = JSON.stringify(user);
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post('http://localhost:3001/user/singin', body, {headers: headers})
        .map((response: Response) => {
          this.token = response.json().token;
          return response.json();
        })
        .catch((error: Response) => Observable.throw(error.json()));     
  }

  logout() {
    this.token = null;
    localStorage.clear();
    this.router.navigate(["/signin"])
  }

  getToken() {    
  }

  isAuthenticated() { 
    /*return true;*/
    this.token = localStorage.getItem('token');
    if(!this.token) return false;
    const body = JSON.stringify({'token': this.token});
    //const body = this.token;
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post('http://localhost:3001/user/isAuthenticated', body, {headers: headers})
      .map((response: Response) => {
        if (response.json().isAuthenticated)
          return true;
        else
          return false;
      })
      .catch((error: Response) => Observable.throw(error.json()));
  }
}
