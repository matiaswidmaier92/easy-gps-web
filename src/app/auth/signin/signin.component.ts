import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";

import { AuthService } from '../auth.service';
import { User } from "app/user/user.model";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  authError: boolean;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSignin(form: NgForm) {
    localStorage.clear();
    const user = new User(form.value.userName, form.value.password);
    this.authService.signinUser(user)
        .subscribe(
            data => {
              this.authError = false;
              localStorage.setItem('token', data.token);
              localStorage.setItem('userId', data.userId);
              this.router.navigateByUrl('/');
            },
            error => {
              this.authError = true;
            }
        );
  }

}
